package com.fslq.dao;

import com.fslq.pojo.EnrollmentPlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnrollmentPlanDao extends JpaRepository<EnrollmentPlan,Long> {
}
