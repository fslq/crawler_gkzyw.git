package com.fslq.dao;

import com.fslq.pojo.JobDetail;
import org.springframework.data.jpa.repository.JpaRepository;

/*jobdetail持久层*/
public interface SchoolJobDetailDao extends JpaRepository<JobDetail,Long> {
}
