package com.fslq.dao;

import com.fslq.pojo.SchoolSpecial;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SchoolSpecialDao extends JpaRepository<SchoolSpecial,Long> {
}
