package com.fslq.dao;

import com.fslq.pojo.SchoolCond;
import org.springframework.data.jpa.repository.JpaRepository;
/*持久层*/
public interface SchoolCondDao  extends JpaRepository<SchoolCond,Integer> {
}
