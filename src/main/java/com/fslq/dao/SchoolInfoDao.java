package com.fslq.dao;

import com.fslq.pojo.SchoolCond;
import com.fslq.pojo.SchoolInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/*info持久层*/
public interface SchoolInfoDao extends JpaRepository<SchoolInfo,Long> {
}
