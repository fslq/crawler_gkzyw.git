package com.fslq.model;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;


public class StartParam {
    private Integer startId;

    public Integer getStartId() {
        return startId;
    }

    public void setStartId(Integer startId) {
        this.startId=startId;
    }
}
