package com.fslq.pipeline;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fslq.pojo.JobDetail;
import com.fslq.service.SchoolService;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import static com.fslq.task.AppProcessor.MAPPER;
import static java.lang.Thread.sleep;
@Component
public class SchoolDetailDataPipeline implements Pipeline {
    @Autowired
    private SchoolService schoolService;
    @Override
    public void process(ResultItems resultItems,Task task) {
        WebDriver driver=new ChromeDriver( new ChromeOptions().addArguments( "--headless" ) );
        List<JobDetail> lists=new ArrayList<>(  );
        try{
           // System.setProperty( "webdriver.chrome.driver","/Volumes/MacSpace/Google/" );
            List<Integer> allSchoolId=schoolService.findAllSchoolId();
            AtomicInteger i=new AtomicInteger();
            allSchoolId.stream().forEach( p->{
                try{
                    System.out.println("学校编号："+i.getAndIncrement() );
                    schoolService.saveAllJobDetail(findJobDetail( driver,p));
                }catch(InterruptedException e){
                    System.out.println( "异常："+e.getMessage() );
                }catch(JsonProcessingException e){
                    e.printStackTrace();
                }
            } );
        }catch(Exception e){
            System.out.println( e.getMessage() );
        } finally {
            //关闭浏览器（这个包括驱动完全退出，会清除内存），close 是只关闭浏览器
            driver.quit();
        }
    }

    @Override
    public boolean equals(Object o) {
        if(this==o) return true;
        if(o==null || getClass()!=o.getClass()) return false;
        SchoolDetailDataPipeline that=(SchoolDetailDataPipeline) o;
        return Objects.equals( schoolService,that.schoolService );
    }

    @Override
    public int hashCode() {
        return Objects.hash( schoolService );
    }

    private JobDetail findJobDetail(WebDriver driver, Integer s_id) throws InterruptedException, JsonProcessingException {
        /**https://static-data.eol.cn/www/school/102/pc_jobdetail.json
         *  "jobrate": { 就业率
         *     "job": {
         *       "1": "94.70" 就业
         *     },
         *     "postgraduate": {
         *       "1": "29.60" 升学
         *     },
         *     "abroad": {
         *       "1": "16.40" 留学
         *     }
         *   },
         *   "gradute": [
         *     {
         *       "school_id": "102",
         *       "female_num": "2497",
         *       "men_num": "2005",
         *       "men_rate": "44.52",
         *     }
         *   ]
         *   "province": {
         *     "4564": {
         *       "province": "中部地区",
         *       "rate": "7.40",
         *   },
         *   "attr": {
         *     "国有企业": "25.30",
         *     "三资企业": "6.90",
         *     "机关": "5.30",
         *     "医疗卫生单位": "2.50",
         *     "中初等教育单位": "0.50",
         *     "科研设计单位": "0.40",
         *     "高等教育单位": "0.10",
         *     "部队": "0.10",
         *     "其他企业": "57.60",
         *     "其他事业单位": "1.00",
         *     "其他": "0.30"
         *   },
         *   },

         */
        WebDriverWait wait=new WebDriverWait( driver,50 );
        //GET PAGE CONTENT
        String url="https://static-data.eol.cn/www/school/"+s_id+"/pc_jobdetail.json";
        driver.get( url );
        sleep( 500 );
        String text=wait.until( ExpectedConditions.presenceOfElementLocated( By.xpath( "/html/body" ) ) ).getText();
        List<HashMap<String, String>> maps_jobrate=new ArrayList<>( );
        HashMap<String, String> map_gradute=new HashMap<>(  );
        List<HashMap<String, String>> map_province=new ArrayList<>( );
        HashMap<String, String> map_attr=new HashMap<>(  );
        //解析Json
        if(isJsonForm( text )){
            JsonNode s=MAPPER.readTree( text );
            JsonNode jobrate=s.get( "jobrate" );
            JsonNode gradute=s.get( "gradute" ).get(0);
            JsonNode province=s.get( "province" );
            JsonNode attr=s.get( "attr" );
            //抽取数据
            if(jobrate.hashCode()!=0 )
                jobrate.forEach( p->{
                    if(p.size()>0)
                        maps_jobrate.add( MAPPER.convertValue( p,HashMap.class ) );
                } );
            if(!(gradute==null)){
                map_gradute=MAPPER.convertValue( gradute,HashMap.class );
            }
            if(province.hashCode()!=0)
                province.forEach( p->{
                    if(p.size()>0)
                        map_province.add( MAPPER.convertValue( p,HashMap.class ) );
                } );
            if(attr.hashCode()!=0 ){
                map_attr=MAPPER.convertValue( attr,HashMap.class );
            }
        }
        return buildJobDetail(maps_jobrate,map_gradute,map_province,map_attr,s_id);
    }
    /*封装*/
    public JobDetail buildJobDetail(List<HashMap<String,String>> maps_jobrate, HashMap<String,String> map_gradute, List<HashMap<String,
            String>> map_province, HashMap<String,String> map_attr, Integer s_id){
        JobDetail jobDetail=new JobDetail();
        jobDetail.setSchoolId( s_id );
        if(maps_jobrate.size()>0)
        {
            jobDetail.setJob( Float.parseFloat(maps_jobrate.get( 0 ).get("1") ));
            jobDetail.setPostgraduate( Float.parseFloat(maps_jobrate.get( 1 ).get("1") ));
            jobDetail.setAbroad( Float.parseFloat(maps_jobrate.get( 2 ).get("1") ));
        }
        if(map_gradute.size()>0){
            jobDetail.setMenNum( Long.parseLong( map_gradute.get( "men_num" ) ) );
            jobDetail.setFemaleNum( Long.parseLong( map_gradute.get( "female_num" ) ) );
            jobDetail.setMenRate( Float.parseFloat( map_gradute.get(  "men_rate") ) );
        }

        if(map_province.size()>0){
            int i=map_province.size();
            if (i>0)
                jobDetail.setOtherArea( Float.parseFloat(  map_province.get( 0 ).get( "rate" )) );
            if ((i>1))
                jobDetail.setCentralArea( Float.parseFloat(  map_province.get( 1 ).get( "rate" )) );
            if (i>2)
                jobDetail.setWestArea( Float.parseFloat(  map_province.get( 2 ).get( "rate" )) );
            if ((i>3))
                jobDetail.setEasternArea( Float.parseFloat(  map_province.get( 3 ).get( "rate" )) );
        }
        if(map_attr.size()>0){
            if (map_attr.containsKey("国有企业"))
                jobDetail.setSoenterprise( Float.parseFloat( map_attr.get( "国有企业" ) ) );
            if (map_attr.containsKey("三资企业"))
                jobDetail.setFfenterprise(  Float.parseFloat( map_attr.get( "三资企业" ) ) );
            if (map_attr.containsKey("机关"))
                jobDetail.setOrgan( Float.parseFloat( map_attr.get( "机关" ) ) );
            if (map_attr.containsKey("医疗卫生单位"))
                jobDetail.setMedicalUnit( Float.parseFloat( map_attr.get( "医疗卫生单位" ) ) );
            if (map_attr.containsKey("中初等教育单位"))
                jobDetail.setSpeunits( Float.parseFloat( map_attr.get( "中初等教育单位" ) ) );
            if (map_attr.containsKey("科研设计单位"))
                jobDetail.setSdunits( Float.parseFloat( map_attr.get( "科研设计单位" ) ) );
            if (map_attr.containsKey("高等教育单位"))
                jobDetail.setHeunits( Float.parseFloat( map_attr.get( "高等教育单位" ) ) );
            if (map_attr.containsKey("部队"))
                jobDetail.setArmy( Float.parseFloat( map_attr.get( "部队" ) ) );
            if (map_attr.containsKey("其他企业"))
                jobDetail.setOEnterprise( Float.parseFloat( map_attr.get( "其他企业" ) ) );
            if (map_attr.containsKey("其他事业单位"))
                jobDetail.setOInstitutions( Float.parseFloat( map_attr.get( "其他事业单位" ) ) );
            if (map_attr.containsKey("其他"))
                jobDetail.setOther( Float.parseFloat( map_attr.get( "其他" ) ) );
        }
        System.out.println(jobDetail);
        return jobDetail;
    }
    public static boolean isJsonForm(String json) {
        boolean result = false;
        try {
            Object obj=JSON.parse(json);
            result = true;
        } catch (Exception e) {
            result=false;
        }
        return result;
    }
}
