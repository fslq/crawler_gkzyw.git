package com.fslq.pipeline;

import com.fasterxml.jackson.databind.JsonNode;
import com.fslq.pojo.SchoolCond;
import com.fslq.service.SchoolService;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import static com.fslq.task.AppProcessor.MAPPER;
import static java.lang.Thread.sleep;
/*Spring数据入库类*/
@Component
public class SchoolCondDataPipeline implements Pipeline {
    @Autowired
    private SchoolService schoolService;
    @Override
    public void process(ResultItems resultItems,Task task) {
        //1.条件查询院校
        //获取网页封装的实体类
        List<SchoolCond> lists=null;
        try{
            lists=searchSchool();
        }catch(IOException e){
            e.printStackTrace();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        //实体信息不为空
        if(lists!=null){
             schoolService.saveAll( lists );
        }
    }
    //  查学校：全部-综合-普通院校-全部
    private List<SchoolCond> searchSchool() throws IOException, InterruptedException {
        // 创建一个 chrome 的浏览器实例
        WebDriver driver=new ChromeDriver( new ChromeOptions().addArguments( "--headless" ) );
        List<SchoolCond> lists=new LinkedList<>();
        try{
            // 设置 chrome 的路径
            //System.setProperty( "webdriver.chrome.driver","C:\\Users\\22966\\AppData\\Local\\Google\\Chrome\\Application\\chromedriver.exe" );
            WebDriverWait wait=new WebDriverWait( driver,50 );
            driver.get( "https://gkcx.eol.cn/school/search?recomschprop=%E7%BB%BC%E5%90%88&argschtype=%E6%99%AE%E9%80%9A%E6%9C%AC%E7%A7%91" );
            //末页
            wait.until( ExpectedConditions.presenceOfElementLocated( By.xpath( "/html/body/div[1]/div/div/div/div/div/div/div[3]/div[1]/div[2]/div[2]/ul/li[11]" ) ) ).click();
            //尾页页码
            int last_page_num=Integer.parseInt( wait.until( ExpectedConditions.presenceOfElementLocated( By.xpath( "//*[@id='root']/div/div/div/div/div/div/div[3]/div[1]/div[2]/div[2]/ul/li[9]" ) ) ).getText() );
            //遍历每一页
            do {
                //GET PAGE CONTENT
                String url="https://api.eol.cn/gkcx/api/?page="+last_page_num+"&school_type=6000&size=20&type=5000&uri=apigkcx/api/school/hotlists";
                driver.get( url );
                sleep( 500 );
                String text=wait.until( ExpectedConditions.presenceOfElementLocated( By.xpath( "/html/body" ) ) ).getText();
                //解析Json
                JsonNode s=MAPPER.readTree( text ).get( "data" ).get( "item" );
                //SAVE SQL
                //存入Map结构
                Map<String, Object> map=null;
                for (JsonNode p : s) {
                    map=MAPPER.convertValue( p,Map.class );
                    //存入数据库对象实例
                    lists.add( saveSchoolCondInfo( map ) );
                }
                last_page_num--;
            } while (last_page_num > 0);
        }catch(Exception e){
            System.out.println( e.getMessage() );
        } finally {
            //关闭浏览器（这个包括驱动完全退出，会清除内存），close 是只关闭浏览器
            driver.quit();
            return lists;
        }
    }
    //封装数据
    private SchoolCond saveSchoolCondInfo(Map<String, Object> map) {
        SchoolCond schoolCond=new SchoolCond();
        schoolCond.setSchool_id( (Integer) map.get( "school_id"  ) );
        schoolCond.setName( (String) map.get( "name"  ) );
        schoolCond.setRank( (Integer) map.get( "rank"  ) );
        schoolCond.setRank_type( (Integer) map.get( "rank_type"  ) );
        schoolCond.setLevel_name( (String) map.get( "level_name"  ) );
        schoolCond.setBelong( (String) map.get( "belong"  ) );
        schoolCond.setCity_name( (String) map.get( "city_name"  ) );
        schoolCond.setCounty_name( (String) map.get( "county_name"  ) );
        schoolCond.setProvince_name( (String) map.get( "province_name"  ) );
        schoolCond.setNature_name( (String) map.get( "nature_name"  ) );
        return schoolCond;
    }
}
