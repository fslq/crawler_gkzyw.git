package com.fslq.pojo;

import javax.persistence.*;

/*条件查询高校数据库实体类*/
@Entity
@Table(name="school_cond")
public class SchoolCond {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;/*编号*/
    private Integer school_id;/*学校编号*/
    private String name;/*学校名称*/
    private Integer rank;/*全国热度排名*/
    private Integer rank_type;/*类别热度排名*/
    private String level_name;/*办学类型*/
    private String belong;/*附属院校*/
    private String city_name;/*所在城市*/
    private String county_name;/*所在区域*/
    private String province_name;/*所在省*/
    private String nature_name;/*院校类型*/


    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id=id;
    }
    public Integer getSchool_id() {
        return school_id;
    }
    public void setSchool_id(Integer school_id) {
        this.school_id=school_id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name=name;
    }
    public Integer getRank() {
        return rank;
    }
    public void setRank(Integer rank) {
        this.rank=rank;
    }
    public Integer getRank_type() {
        return rank_type;
    }
    public void setRank_type(Integer rank_type) {
        this.rank_type=rank_type;
    }
    public String getLevel_name() {
        return level_name;
    }
    public void setLevel_name(String level_name) {
        this.level_name=level_name;
    }
    public String getBelong() {
        return belong;
    }
    public void setBelong(String belong) {
        this.belong=belong;
    }
    public String getCity_name() {
        return city_name;
    }
    public void setCity_name(String city_name) {
        this.city_name=city_name;
    }
    public String getCounty_name() {
        return county_name;
    }
    public void setCounty_name(String county_name) {
        this.county_name=county_name;
    }
    public String getProvince_name() {
        return province_name;
    }
    public void setProvince_name(String province_name) {
        this.province_name=province_name;
    }
    public String getNature_name() {
        return nature_name;
    }
    public void setNature_name(String nature_name) {
        this.nature_name=nature_name;
    }

    @Override
    public String toString() {
        return "SchoolCond{"+
                "id="+id+
                ", school_id="+school_id+
                ", name='"+name+'\''+
                ", rank="+rank+
                ", rank_type="+rank_type+
                ", level_name='"+level_name+'\''+
                ", belong='"+belong+'\''+
                ", city_name='"+city_name+'\''+
                ", county_name='"+county_name+'\''+
                ", province_name='"+province_name+'\''+
                ", nature_name='"+nature_name+'\''+
                '}';
    }
}
