package com.fslq.pojo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "enrollment_plan")
public class EnrollmentPlan {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private long schoolId;
  private String province;
  private String subjectType;
  private String batch;
  private String specialName;
  private String specialType;
  private long planNum;

  @Override
  public boolean equals(Object o) {
    if(this==o) return true;
    if(o==null || getClass()!=o.getClass()) return false;
    EnrollmentPlan that=(EnrollmentPlan) o;
    return id==that.id &&
            schoolId==that.schoolId &&
            planNum==that.planNum &&
            Objects.equals( province,that.province ) &&
            Objects.equals( subjectType,that.subjectType ) &&
            Objects.equals( batch,that.batch ) &&
            Objects.equals( specialName,that.specialName ) &&
            Objects.equals( specialType,that.specialType );
  }

  @Override
  public int hashCode() {
    return Objects.hash( id,schoolId,province,subjectType,batch,specialName,specialType,planNum );
  }

  @Override
  public String toString() {
    return "EnrollmentPlan{" +
            "id=" + id +
            ", schoolId=" + schoolId +
            ", province=" + province +
            ", subjectType='" + subjectType + '\'' +
            ", batch='" + batch + '\'' +
            ", specialName='" + specialName + '\'' +
            ", specialType='" + specialType + '\'' +
            ", planNum=" + planNum +
            '}';
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getSchoolId() {
    return schoolId;
  }

  public void setSchoolId(long schoolId) {
    this.schoolId = schoolId;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getSubjectType() {
    return subjectType;
  }

  public void setSubjectType(String subjectType) {
    this.subjectType = subjectType;
  }

  public String getBatch() {
    return batch;
  }

  public void setBatch(String batch) {
    this.batch = batch;
  }

  public String getSpecialName() {
    return specialName;
  }

  public void setSpecialName(String specialName) {
    this.specialName = specialName;
  }

  public String getSpecialType() {
    return specialType;
  }

  public void setSpecialType(String specialType) {
    this.specialType = specialType;
  }

  public long getPlanNum() {
    return planNum;
  }

  public void setPlanNum(long planNum) {
    this.planNum = planNum;
  }
}
