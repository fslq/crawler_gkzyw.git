package com.fslq.pojo;

import javax.persistence.*;

@Entity
@Table(name="job_detail")
public class JobDetail {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private long schoolId;
  private float job;
  private float postgraduate;
  private float abroad;
  private long menNum;
  private long femaleNum;
  private float menRate;
  private float otherArea;
  private float centralArea;
  private float westArea;
  private float easternArea;
  private float soenterprise;
  private float ffenterprise;
  private float organ;
  private float medicalUnit;
  private float speunits;
  private float sdunits;
  private float heunits;
  private float army;
  private float oEnterprise;
  private float oInstitutions;
  private float other;

  @Override
  public String toString() {
    return "JobDetail{" +
            "id=" + id +
            ", schoolId=" + schoolId +
            ", job=" + job +
            ", postgraduate=" + postgraduate +
            ", abroad=" + abroad +
            ", menNum=" + menNum +
            ", femaleNum=" + femaleNum +
            ", menRate=" + menRate +
            ", otherArea=" + otherArea +
            ", centralArea=" + centralArea +
            ", westArea=" + westArea +
            ", easternArea=" + easternArea +
            ", soenterprise=" + soenterprise +
            ", ffenterprise=" + ffenterprise +
            ", organ=" + organ +
            ", medicalUnit=" + medicalUnit +
            ", speunits=" + speunits +
            ", sdunits=" + sdunits +
            ", heunits=" + heunits +
            ", army=" + army +
            ", oEnterprise=" + oEnterprise +
            ", oInstitutions=" + oInstitutions +
            ", other=" + other +
            '}';
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getSchoolId() {
    return schoolId;
  }

  public void setSchoolId(long schoolId) {
    this.schoolId = schoolId;
  }


  public double getJob() {
    return job;
  }

  public void setJob(float job) {
    this.job = job;
  }


  public double getPostgraduate() {
    return postgraduate;
  }

  public void setPostgraduate(float postgraduate) {
    this.postgraduate = postgraduate;
  }


  public double getAbroad() {
    return abroad;
  }

  public void setAbroad(float abroad) {
    this.abroad = abroad;
  }


  public long getMenNum() {
    return menNum;
  }

  public void setMenNum(long menNum) {
    this.menNum = menNum;
  }


  public long getFemaleNum() {
    return femaleNum;
  }

  public void setFemaleNum(long femaleNum) {
    this.femaleNum = femaleNum;
  }


  public double getMenRate() {
    return menRate;
  }

  public void setMenRate(float menRate) {
    this.menRate = menRate;
  }


  public double getOtherArea() {
    return otherArea;
  }

  public void setOtherArea(float otherArea) {
    this.otherArea = otherArea;
  }


  public double getCentralArea() {
    return centralArea;
  }

  public void setCentralArea(float centralArea) {
    this.centralArea = centralArea;
  }


  public double getWestArea() {
    return westArea;
  }

  public void setWestArea(float westArea) {
    this.westArea = westArea;
  }


  public double getEasternArea() {
    return easternArea;
  }

  public void setEasternArea(float easternArea) {
    this.easternArea = easternArea;
  }


  public double getSoenterprise() {
    return soenterprise;
  }

  public void setSoenterprise(float soenterprise) {
    this.soenterprise = soenterprise;
  }


  public double getFfenterprise() {
    return ffenterprise;
  }

  public void setFfenterprise(float ffenterprise) {
    this.ffenterprise = ffenterprise;
  }


  public double getOrgan() {
    return organ;
  }

  public void setOrgan(float organ) {
    this.organ = organ;
  }


  public double getMedicalUnit() {
    return medicalUnit;
  }

  public void setMedicalUnit(float medicalUnit) {
    this.medicalUnit = medicalUnit;
  }


  public double getSpeunits() {
    return speunits;
  }

  public void setSpeunits(float speunits) {
    this.speunits = speunits;
  }


  public double getSdunits() {
    return sdunits;
  }

  public void setSdunits(float sdunits) {
    this.sdunits = sdunits;
  }


  public double getHeunits() {
    return heunits;
  }

  public void setHeunits(float heunits) {
    this.heunits = heunits;
  }


  public double getArmy() {
    return army;
  }

  public void setArmy(float army) {
    this.army = army;
  }


  public double getOEnterprise() {
    return oEnterprise;
  }

  public void setOEnterprise(float oEnterprise) {
    this.oEnterprise = oEnterprise;
  }


  public double getOInstitutions() {
    return oInstitutions;
  }

  public void setOInstitutions(float oInstitutions) {
    this.oInstitutions = oInstitutions;
  }


  public double getOther() {
    return other;
  }

  public void setOther(float other) {
    this.other = other;
  }

}
