package com.fslq.pojo;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name="school_info")
public class SchoolInfo {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonProperty
  private long id;
  private long schoolId;
  private String f985;
  private String f211;
  private long numSubject;
  private long numMaster;
  private long numDoctor;
  private String dualClassName;
  private long numLab;
  private float learningIndex;
  private float overallScore;
  private float liveIndex;
  private float jobIndex;
  private long numLibrary;
  private long createDate;
  private float area;
  private String schoolSite;
  private String phone;
  private String imgUrl;
  private String content;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id=id;
  }

  public long getSchoolId() {
    return schoolId;
  }

  public void setSchoolId(long schoolId) {
    this.schoolId=schoolId;
  }

  public String getF985() {
    return f985;
  }

  public void setF985(String f985) {
    this.f985=f985;
  }

  public String getF211() {
    return f211;
  }

  public void setF211(String f211) {
    this.f211=f211;
  }

  public long getNumSubject() {
    return numSubject;
  }

  public void setNumSubject(long numSubject) {
    this.numSubject=numSubject;
  }

  public long getNumMaster() {
    return numMaster;
  }

  public void setNumMaster(long numMaster) {
    this.numMaster=numMaster;
  }

  public long getNumDoctor() {
    return numDoctor;
  }

  public void setNumDoctor(long numDoctor) {
    this.numDoctor=numDoctor;
  }

  public String getDualClassName() {
    return dualClassName;
  }

  public void setDualClassName(String dualClassName) {
    this.dualClassName=dualClassName;
  }

  public long getNumLab() {
    return numLab;
  }

  public void setNumLab(long numLab) {
    this.numLab=numLab;
  }

  public float getLearningIndex() {
    return learningIndex;
  }

  public void setLearningIndex(float learningIndex) {
    this.learningIndex=learningIndex;
  }

  public float getOverallScore() {
    return overallScore;
  }

  public void setOverallScore(float overallScore) {
    this.overallScore=overallScore;
  }

  public float getLiveIndex() {
    return liveIndex;
  }

  public void setLiveIndex(float liveIndex) {
    this.liveIndex=liveIndex;
  }

  public float getJobIndex() {
    return jobIndex;
  }

  public void setJobIndex(float jobIndex) {
    this.jobIndex=jobIndex;
  }

  public long getNumLibrary() {
    return numLibrary;
  }

  public void setNumLibrary(long numLibrary) {
    this.numLibrary=numLibrary;
  }

  public long getCreateDate() {
    return createDate;
  }

  public void setCreateDate(long createDate) {
    this.createDate=createDate;
  }

  public float getArea() {
    return area;
  }

  public void setArea(float area) {
    this.area=area;
  }

  public String getSchoolSite() {
    return schoolSite;
  }

  public void setSchoolSite(String schoolSite) {
    this.schoolSite=schoolSite;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone=phone;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl=imgUrl;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content=content;
  }

  @Override
  public String toString() {
    return "SchoolInfo{"+
            "id="+id+
            ", schoolId="+schoolId+
            ", f985='"+f985+'\''+
            ", f211='"+f211+'\''+
            ", numSubject="+numSubject+
            ", numMaster="+numMaster+
            ", numDoctor="+numDoctor+
            ", dualClassName='"+dualClassName+'\''+
            ", numLab="+numLab+
            ", learningIndex="+learningIndex+
            ", overallScore="+overallScore+
            ", liveIndex="+liveIndex+
            ", jobIndex="+jobIndex+
            ", numLibrary="+numLibrary+
            ", createDate="+createDate+
            ", area="+area+
            ", schoolSite='"+schoolSite+'\''+
            ", phone='"+phone+'\''+
            ", imgUrl='"+imgUrl+'\''+
            ", content='"+content+'\''+
            '}';
  }
}
