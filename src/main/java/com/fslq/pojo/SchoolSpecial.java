package com.fslq.pojo;

import javax.persistence.*;

@Entity
@Table(name="school_special")
public class SchoolSpecial{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private long schoolId;
  private String specials;


  @Override
  public String toString() {
    return "SchoolSpecial{" +
            "id=" + id +
            ", schoolId=" + schoolId +
            ", specials='" + specials + '\'' +
            '}';
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public SchoolSpecial() {
  }

  public Long getSchoolId() {
    return schoolId;
  }

  public void setSchoolId(long schoolId) {
    this.schoolId = schoolId;
  }


  public String getSpecials() {
    return specials;
  }

  public void setSpecials(String specials) {
    this.specials = specials;
  }

}
