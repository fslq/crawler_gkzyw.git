package com.fslq.impl;

import com.fslq.dao.*;
import com.fslq.pojo.*;
import com.fslq.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*实现类*/
@Service
public class SchoolServiceImpl implements SchoolService {
    @Autowired
    private SchoolCondDao schoolCondDao;
    @Autowired
    private SchoolInfoDao schoolInfoDao;
    @Autowired
    private SchoolJobDetailDao schoolJobDetailDao;
    @Autowired
    private SchoolSpecialDao schoolSpecialDao;
    @Autowired
    private EnrollmentPlanDao enrollmentPlanDao;
    @Override
    @Transactional
    public void saveAllSpecials(List<SchoolSpecial> lists) {
        schoolSpecialDao.saveAll(lists);
    }
    @Override
    @Transactional//注入事务
    public void saveAllInfo(List<SchoolInfo> lists) {
        schoolInfoDao.saveAll( lists );
    }

    @Override
    public void saveAllJobDetail(JobDetail lists) {
        schoolJobDetailDao.save( lists );
    }

    @Override
    public List<SchoolInfo> findAllInfo(SchoolInfo schoolInfo) {
        Example example=Example.of( schoolInfo );
        return schoolInfoDao.findAll(example);
    }

    @Override
    public List<JobDetail> findAllJobDetail(JobDetail jobDetail) {
        Example example=Example.of( jobDetail );
        return schoolJobDetailDao.findAll(example);
    }

    @Override
    public List<Integer> findAllSchoolId() {
        List<Integer> school_ids=new ArrayList<>(  );
        schoolCondDao.findAll( ).stream().forEach( p->{
            school_ids.add( p.getSchool_id() );
        } );
        return school_ids;
    }

    @Override
    @Transactional//注入事务
    public void saveAll(List<SchoolCond> lists) {
        schoolCondDao.saveAll( lists );
    }
    @Override
    public List<SchoolCond> findAll(SchoolCond schoolCond) {
        Example<SchoolCond> of=Example.of( schoolCond );
        return schoolCondDao.findAll(of);
    }

    @Override
    public void saveAllEnrollmentPlan(Set<EnrollmentPlan> enrollmentPlan) {
        enrollmentPlanDao.saveAll(enrollmentPlan);
    }

    @Override
    public List<Long> IsExistAllId() {
        Set<Long> enroll_list=new HashSet<>();
        List<Long> list=new ArrayList<>();
        enrollmentPlanDao.findAll().forEach(p->enroll_list.add(p.getSchoolId()));
        schoolSpecialDao.findAll().forEach(p->{
            if (!enroll_list.contains(p.getSchoolId())) {//不包含
                list.add(p.getSchoolId());
            }
        });
        return list;//  未爬取
    }
}
