package com.fslq.service;

import com.fslq.pojo.*;

import java.util.List;
import java.util.Set;

/*业务层*/
public interface SchoolService {
    //保存一级学科专业
    void saveAllSpecials(List<SchoolSpecial> lists);
    //保存info
    void saveAllInfo(List<SchoolInfo> lists);
    //保存jobdetail
    void saveAllJobDetail(JobDetail lists);
    //查询所有Info
    List<SchoolInfo> findAllInfo(SchoolInfo schoolInfo);
    //查询所有jobdetail
    List<JobDetail> findAllJobDetail(JobDetail jobDetail);
    //查询所有学校ID
    List<Integer> findAllSchoolId();
    //保存SchoolCond
    void saveAll(List<SchoolCond> lists);
    //查询所有SchoolCond
    List<SchoolCond> findAll(SchoolCond schoolCond);
    //保存招生计划
    void saveAllEnrollmentPlan(Set<EnrollmentPlan> enrollmentPlan);
    //IsExistAllId:enroll..在cond表中的id
    List<Long> IsExistAllId();
}
