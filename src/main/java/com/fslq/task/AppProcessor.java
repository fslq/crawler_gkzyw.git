package com.fslq.task;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fslq.pipeline.*;
import com.fslq.pojo.SchoolCond;
import com.fslq.pojo.SchoolInfo;
import com.fslq.service.SchoolService;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import java.io.IOException;
import java.util.*;

import static java.lang.Thread.sleep;

/*相关操作*/
@Component
public class AppProcessor implements PageProcessor {
    @Autowired
    SchoolCondDataPipeline schoolCondDataPipeline;
    @Autowired
    SchoolInfoDataPipeline schoolInfoDataPipeline;
    @Autowired
    SchoolDetailDataPipeline schoolDetailDataPipeline;
    @Autowired
    SchoolSpecialDataPipeline schoolSpecialDataPipeline;
    @Autowired
    EnrollmentPlanDataPipeline enrollmentPlanDataPipeline;
    public static final ObjectMapper MAPPER=new ObjectMapper();//解析json数据
    @Override
    public void process(Page page) {
    }

    private Site site=Site.me()
            .setCharset( "utf8" )
            .setCycleRetryTimes( 3 )
            .setSleepTime( 10* 1000 );
            //.addHeader( "Connection","keep-alive" );
    @Override
    public Site getSite() {
        return site;
    }
    //执行爬虫
    @Scheduled(initialDelay=1000,fixedDelay=10*1000)//定时任务不该有参数
    public void fun() {
        Spider.create( new AppProcessor() )
                .addUrl( "https://gkcx.eol.cn/school/search?recomschprop=%E7%BB%BC%E5%90%88&argschtype=%E6%99%AE%E9%80%9A%E6%9C%AC%E7%A7%91" )//设置爬取页面
                .thread(10)
                //.addPipeline( schoolCondDataPipeline )//数据入库
                //.addPipeline( schoolInfoDataPipeline )
                //.addPipeline( schoolDetailDataPipeline )
                //.addPipeline( schoolSpecialDataPipeline )
                .addPipeline(enrollmentPlanDataPipeline)
                .run();//执行爬虫
    }
}
