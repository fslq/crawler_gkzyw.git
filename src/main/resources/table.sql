drop database if exists colleges;
create schema colleges collate utf8_general_ci;
use colleges;
create table if not exists job_detail
(
    id int(2) primary key,
    school_id int(2) unique ,
    job float null comment '就业率',
    postgraduate float null comment '升学率',
    abroad float null comment '留学率',
    men_num int(2) null comment '男生人数',
    female_num int(2) null comment '女生人数',
    men_rate float null comment '男生比例',
    other_area float null comment '其他地区就业率',
    central_area float null comment '中部地区就业率',
    west_area float null comment '西部地区..',
    eastern_area float null comment '东部地区..',
    soenterprise float null comment '国有企业',
    ffenterprise float null comment '三资企业',
    organ float null comment '机关部门',
    medical_unit float null comment '医疗卫生单位',
    speunits float null comment '中初等教育单位',
    sdunits float null comment '科研设计单位',
    heunits float null comment '高等教育单位',
    army float null comment '部队',
    o_enterprise float null comment '其他企业',
    o_institutions float null comment '其他事业单位',
    other float null comment '其他'
);

create table school_cond
(
    id int(2) auto_increment
        primary key,
    school_id int(2) unique null,
    name varchar(60) null,
    `rank` int(2) null,
    rank_type int(2) null,
    level_name varchar(18) null,
    belong varchar(45) null,
    city_name varchar(21) null,
    county_name varchar(18) null,
    province_name varchar(18) null,
    nature_name varchar(18) null);

create table school_info
(
    id int(2) auto_increment
        primary key,
    school_id int(2) unique null,
    f985 char null comment '985',
    f211 char null comment '211',
    num_subject int(1) null comment '重点学科',
    num_master int(1) null comment '硕士点',
    num_doctor int(1) null,
    dual_class_name varchar(9) null comment '双一流',
    num_lab int(1) null comment '重点实验室',
    learning_index float null comment '学习指数',
    overall_score float null comment '综合评分',
    live_index float null comment '生活指数',
    job_index float null comment '就业指数',
    num_library int(2) null,
    create_date int(2) null comment '建校时间',
    area float null comment '占地面积',
    school_site varchar(100) null comment '官网',
    phone varchar(50) null comment '电话',
    img_url varchar(100) null comment '图片',
    content varchar(500) null comment '简介'
);

create table if not exists school_special(
    id int(2) auto_increment primary key ,
    school_id int(2) unique null ,
    specials varchar(210) null comment '一级学科'
);

create table if not exists enrollment_plan(
    id int(2) primary key auto_increment,
    school_id int(2) comment '学校编号',
    province varchar(30) comment '生源地',
    subject_type varchar(30) comment '科类',
    batch varchar(35) comment '批次',
    special_name varchar(500) comment '专业名称',
    special_type varchar(40) comment '专业门类',
    plan_num int(2) comment '招生人数'
)comment '招生计划';

delete from school_cond where school_id in (
    select school_id from job_detail where abroad=0 and female_num=0 and men_num=0 and postgraduate=0 or job=0);
delete from school_info where school_id in (
    select school_id from job_detail where abroad=0 and female_num=0 and men_num=0 and postgraduate=0 or job=0);
delete from school_special where school_id not in (
    select school_id from job_detail);
delete from job_detail where abroad=0 and female_num=0 and men_num=0 and postgraduate=0 or job=0;



select * from school_cond A join job_detail B on A.school_id =B.school_id where A.school_id in (
    select school_id from job_detail where female_num=0 and men_num=0
    );

select school_id from school_cond where name like '%学院' and name in (
    select name from school_cond A join job_detail B on A.school_id =B.school_id where A.school_id in (
        select school_id from job_detail where female_num=0 and men_num=0
    )
);

delete from school_special where school_id in (
    select school_id from school_cond where name like '%学院' and name in (
        select name from school_cond A join job_detail B on A.school_id =B.school_id where A.school_id in (
            select school_id from job_detail where female_num=0 and men_num=0
        )
    )
    );
delete from job_detail where school_id not in (
    select school_id from school_special
    );
delete from school_cond where school_id not in (
    select school_id from school_special
    );


delete from school_cond where school_id in (
    select school_id from job_detail where o_institutions=0);
delete from school_info where school_id in (
    select school_id from job_detail where o_institutions=0);
delete from school_special where school_id not in (
    select school_id from job_detail);
delete from school_info where school_id not in (
    select school_id from job_detail);
delete from job_detail where o_institutions=0;



delete from enrollment_plan where id>3031;

select  * from school_cond where school_id=421;
select distinct school_id from enrollment_plan;

select count(school_id) from school_cond;

delete from enrollment_plan  where school_id=244;
select  * from enrollment_plan where plan_num=0;
delete from enrollment_plan where plan_num=0;
select count(distinct school_id) from enrollment_plan;